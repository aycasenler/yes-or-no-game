package com.crescent.yesorno

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.util.*

class CardStackAdapter(
    private var datas: List<GameScreenFragment.Data> = emptyList()
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name_eng: TextView = view.findViewById(R.id.item_name_eng)
        var name_tr: TextView = view.findViewById(R.id.item_name_tr)
        var image: ImageView = view.findViewById(R.id.item_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cards_design, parent, false))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = datas[position]

        holder.name_tr.text = data.name_tr
        holder.name_eng.text = data.name_eng

        Picasso.get().load(data.image_url).into(holder.image);

        holder.itemView.setOnClickListener { v ->
            Toast.makeText(v.context, data.name_eng, Toast.LENGTH_SHORT).show()
        }
    }
}