package com.crescent.yesorno.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import androidx.recyclerview.widget.RecyclerView
import com.crescent.yesorno.GameScreenFragment
import com.crescent.yesorno.R
import com.squareup.picasso.Picasso

class CardStackAdapter(
    private var datas: List<GameScreenFragment.Data> = emptyList()
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name_eng: TextView = view.findViewById(R.id.item_name_eng)
        var name_tr: TextView = view.findViewById(R.id.item_name_tr)
        var image: ImageView = view.findViewById(R.id.item_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cards_design, parent, false))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = datas[position]

        if (data.name_tr.isNullOrEmpty()) {
            holder.name_tr.visibility = View.GONE
        } else {
            holder.name_tr.visibility = View.VISIBLE
            holder.name_tr.text = data.name_tr
        }
        if (data.name_eng.isNullOrEmpty()) {
            holder.name_eng.visibility = View.GONE
        } else {
            holder.name_eng.visibility = View.VISIBLE
            holder.name_eng.text = data.name_eng
        }

        Picasso.get().load(data.image_url).into(holder.image);

        holder.itemView.setOnClickListener { v ->
            Toast.makeText(v.context, data.name_eng, Toast.LENGTH_SHORT).show()
        }
    }
}