package com.crescent.yesorno


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders

class SplashScreenActivity : AppCompatActivity(){
    private val SPLASH_TIME_OUT : Long =5000
    lateinit var  circularFillableLoaders : CircularFillableLoaders
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        circularFillableLoaders = findViewById(R.id.circularFillableLoaders)



        Handler().postDelayed({

            startActivity(Intent(this,MainActivity::class.java))

            finish()
        }, SPLASH_TIME_OUT)
    }
}