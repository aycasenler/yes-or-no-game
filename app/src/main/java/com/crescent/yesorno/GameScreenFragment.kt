package com.crescent.yesorno

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.crescent.yesorno.Adapters.CardStackAdapter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.firebase.firestore.FirebaseFirestore
import com.ldoublem.loadingviewlib.view.LVGhost
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.custom_dialog.view.*

import java.util.*

class GameScreenFragment : Fragment(), View.OnClickListener, CardStackListener {

    lateinit var backBtn: ImageButton
    lateinit var startPauseButton: ImageButton
    lateinit var scoreTxt: TextView
    lateinit var passTxt: TextView
    lateinit var categoriesTxt: TextView
    lateinit var db: FirebaseFirestore
    lateinit var foreignMovieList: MutableList<String>
    lateinit var foreignMovieDataList: MutableList<Data>
    lateinit var turkishMovieList: MutableList<String>
    lateinit var turkishMovieDataList: MutableList<Data>
    lateinit var turkishSeriesList: MutableList<String>
    lateinit var turkishSeriesDataList: MutableList<Data>
    lateinit var foreignSeriesList: MutableList<String>
    lateinit var foreignSeriesDataList: MutableList<Data>
    lateinit var foreignSingerList: MutableList<String>
    lateinit var foreignSingerDataList: MutableList<Data>
    lateinit var turkishSingerList: MutableList<String>
    lateinit var turkishSingerDataList: MutableList<Data>
    lateinit var turkishActorsDataList: MutableList<Data>
    lateinit var turkishActorsList: MutableList<String>
    lateinit var foreignActorsDataList: MutableList<Data>
    lateinit var foreignActorsList: MutableList<String>
    lateinit var cardStackView: CardStackView
    lateinit var countDownText: TextView
    lateinit var countDownTimer: CountDownTimer
    lateinit var blurLayout: LinearLayout
    private var timerRunning: Boolean = false
    private var timeRemaining: Long = 0
    private val manager by lazy { CardStackLayoutManager(context, this) }
    private var score = 0
    private var swipeCount = 0
    private var pass = 3
    private var listSize = 0
    lateinit var progressBar: LVGhost
    lateinit var fullscreenAdView: InterstitialAd

    lateinit var choice: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.game_screen_fragment, container, false)
        initView(view)
        cardStackViewSettings()
        interstitalAd()
        choice = arguments?.getString("choosenCategory").toString()
        db = FirebaseFirestore.getInstance()
        getQuestions(choice)

        MobileAds.initialize(context)
        backPressHandler()
        return view
    }
    private fun initView(view: View) {
        backBtn = view.findViewById(R.id.back_btn)
        scoreTxt = view.findViewById(R.id.score_txt)
        passTxt = view.findViewById(R.id.pass_txt)
        categoriesTxt = view.findViewById(R.id.categories_txt)
        progressBar = view.findViewById(R.id.progress_bar)
        blurLayout = view.findViewById(R.id.blur_layout)
        startPauseButton = view.findViewById(R.id.start_pause_btn)
        foreignMovieList = mutableListOf()
        foreignMovieDataList = mutableListOf()
        turkishMovieList = mutableListOf()
        turkishMovieDataList = mutableListOf()
        turkishSeriesList = mutableListOf()
        turkishSeriesDataList = mutableListOf()
        foreignSeriesList = mutableListOf()
        foreignSeriesDataList = mutableListOf()
        foreignSingerList = mutableListOf()
        foreignSingerDataList = mutableListOf()
        turkishSingerList = mutableListOf()
        turkishSingerDataList = mutableListOf()
        foreignActorsList = mutableListOf()
        foreignActorsDataList = mutableListOf()
        turkishActorsList = mutableListOf()
        turkishActorsDataList = mutableListOf()
        cardStackView = view.findViewById(R.id.card_stack_view)
        countDownText = view.findViewById(R.id.countdown_txt)

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setViewColor(Color.WHITE)
        progressBar.setHandColor(Color.BLACK)
        progressBar.startAnim()
        progressBar.isClickable = false
        blurLayout.isClickable =false

        startPauseButton.setOnClickListener(this)
        backBtn.setOnClickListener(this)

    }
    private fun startTimer() {
        countDownTimer = object : CountDownTimer(90000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                var minutes = (millisUntilFinished / 1000) / 60
                var seconds = (millisUntilFinished / 1000) % 60
                var timeLeftFormatted =
                    String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
                countDownText.text = timeLeftFormatted
                timeRemaining = millisUntilFinished

            }

            override fun onFinish() {
                if (Locale.getDefault().language == "tr"){
                    view?.let { finishDialog(it, "Süre Bitti!" + "\nPuanınız : $score", false) }
                }else{
                    view?.let { finishDialog(it, "Time is up!" + "\nScore : $score", false) }
                }
            }
        }
        countDownTimer.start()
        timerRunning = true


    }

    private fun pauseTimer() {
        countDownTimer.cancel()
        timerRunning = false
        startPauseButton.setImageResource(R.drawable.play)
        backBtn.isClickable = false
        cardStackView.isClickable = false

        manager.setSwipeableMethod(SwipeableMethod.None)
        cardStackView.layoutManager = manager

    }

    private fun resumeTimer() {
        //TODO
        countDownTimer = object : CountDownTimer(timeRemaining, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                var minutes = (millisUntilFinished / 1000) / 60
                var seconds = (millisUntilFinished / 1000) % 60
                var timeLeftFormatted =
                    String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
                countDownText.text = timeLeftFormatted
                timeRemaining = millisUntilFinished


            }

            override fun onFinish() {
                if (Locale.getDefault().language == "tr"){
                    view?.let { finishDialog(it, "Süre Bitti!" + "\nPuanınız : $score", false) }
                }else{
                    view?.let { finishDialog(it, "Time is up!" + "\nScore : $score", false) }
                }

            }
        }
        countDownTimer.start()
        timerRunning = true
        startPauseButton.setImageResource(R.drawable.stop)
        backBtn.isClickable = true

        manager.setSwipeableMethod(SwipeableMethod.Manual)
        //  cardStackView.layoutManager = manager

    }

    private fun getQuestions(choice: String) {
        when (choice) {
            "TurkishMovies" -> {
                getTurkishMovies()
                categoriesTxt.text = resources.getString(R.string.turkish_movies)
            }

            "ForeignMovies" -> {
                getForeignMovies()
                categoriesTxt.text = resources.getString(R.string.foreign_movies)
            }
            "TurkishSinger" -> {
                getTurkishSinger()
                categoriesTxt.text = resources.getString(R.string.turkish_singers)
            }

            "ForeignSinger" -> {
                getForeignSinger()
                categoriesTxt.text = resources.getString(R.string.foreign_singers)
            }
            "TurkishSeries" -> {
                categoriesTxt.text = resources.getString(R.string.turkish_series)
                getTurkishSeries()
            }
            "ForeignSeries" -> {
                getForeignSeries()
                categoriesTxt.text = resources.getString(R.string.foreign_series)
            }
            "TurkishActors" -> {
                getTurkishActors()
                categoriesTxt.text = resources.getString(R.string.turkish_actors)
            }
            "ForeignActors" -> {
                getForeignActors()
                categoriesTxt.text = resources.getString(R.string.foreign_actors)
            }
        }
    }




    private fun cardStackViewSettings() {

        manager.setStackFrom(StackFrom.Top)
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)
        manager.setScaleInterval(0.90f)
        manager.setMaxDegree(20.0f)
        manager.setSwipeThreshold(0.3f)

        cardStackView.layoutManager = manager


    }

    private fun getForeignMovies() {
        db.collection("Categories").document("Movies").collection("ForeignMovies")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    foreignMovieList.add(document.id)
                    if (Locale.getDefault().language == "tr"){
                        val movie = Data(
                            document.data["image_url"] as String,
                            document.data["name_eng"] as String,
                            document.data["name_tr"] as String
                        )
                        foreignMovieDataList.add(movie)
                        foreignMovieDataList.shuffle()
                    }else{
                        val movie = Data(
                            document.data["image_url"] as String,
                            document.data["name_eng"] as String
                        )
                        foreignMovieDataList.add(movie)
                        foreignMovieDataList.shuffle()
                    }




                }
                val selectTentDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTentDataList.add(foreignMovieDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTentDataList)

                listSize = selectTentDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getTurkishMovies() {
        db.collection("Categories").document("Movies").collection("TurkishMovies")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    turkishMovieList.add(document.id)

                    var movie = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String
                    )
                    turkishMovieDataList.add(movie)
                    turkishMovieDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(turkishMovieDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getForeignSeries() {
        db.collection("Categories").document("Series").collection("ForeignSeries")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    foreignSeriesList.add(document.id)
                    if (Locale.getDefault().language == "tr"){
                        val series = Data(
                            document.data["image_url"] as String,
                            document.data["name_eng"] as String,
                            document.data["name_tr"] as String
                        )
                        foreignSeriesDataList.add(series)
                        foreignSeriesDataList.shuffle()
                    }else{
                        val series = Data(
                            document.data["image_url"] as String,
                            document.data["name_eng"] as String
                        )
                        foreignSeriesDataList.add(series)
                        foreignSeriesDataList.shuffle()
                    }


                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(foreignSeriesDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }

    }

    private fun getTurkishSeries() {
        db.collection("Categories").document("Series").collection("TurkishSeries")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    turkishSeriesList.add(document.id)

                    var series = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String
                    )
                    turkishSeriesDataList.add(series)
                    turkishSeriesDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(turkishSeriesDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getForeignSinger() {
        db.collection("Categories").document("Singers").collection("ForeignSingers")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    foreignSingerList.add(document.id)

                    var singer = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String

                    )
                    foreignSingerDataList.add(singer)
                    foreignSingerDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(foreignSingerDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getTurkishSinger() {
        db.collection("Categories").document("Singers").collection("TurkishSingers")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    turkishSingerList.add(document.id)

                    var singer = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String
                    )
                    turkishSingerDataList.add(singer)
                    turkishSingerDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(turkishSingerDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getForeignActors() {
        db.collection("Categories").document("Actors").collection("ForeignActors")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    foreignActorsList.add(document.id)

                    var actors = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String
                    )
                    foreignActorsDataList.add(actors)
                    foreignActorsDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(foreignActorsDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    private fun getTurkishActors() {
        db.collection("Categories").document("Actors").collection("TurkishActors")
            .get()
            .addOnSuccessListener { result ->
                startTimer()
                for (document in result) {
                    turkishActorsList.add(document.id)

                    var actors = Data(
                        document.data["image_url"] as String,
                        document.data["name_eng"] as String,
                        document.data["name_tr"] as String
                    )
                    turkishActorsDataList.add(actors)
                    turkishActorsDataList.shuffle()

                }
                val selectTenDataList: MutableList<Data> = mutableListOf()
                for (i in 1..15) {
                    selectTenDataList.add(turkishActorsDataList[i])
                }

                cardStackView.adapter = CardStackAdapter(selectTenDataList)

                listSize = selectTenDataList.size

                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
            }
            .addOnFailureListener { exception ->
            }
    }

    data class Data(
        val image_url: String = "",
        val name_eng: String = "",
        val name_tr: String = ""
    )

    override fun onClick(v: View?) {
        when (v?.id) {
            startPauseButton.id -> {
                if (timerRunning) {
                    pauseTimer()
                } else {
                    resumeTimer()
                }
            }
            backBtn.id -> {
                fullscreenAdView.show()
                Navigation.findNavController(v).navigateUp()
            }
        }
    }

    private fun finishDialog(view: View, message: String, visible: Boolean) {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mAlertDialog.window!!.setLayout(1000, 1400)
        mDialogView.dialog_txt.text = message
        pauseTimer()

        if (visible) {
            mDialogView.ad_btn.visibility = View.VISIBLE
            mDialogView.ad_txt.visibility = View.VISIBLE
            mDialogView.ad_btn.setOnClickListener {
                rewardAd(view)
                blurLayout.visibility = View.VISIBLE
                progressBar.startAnim()
                mAlertDialog.dismiss()
            }
        } else {
            mDialogView.ad_btn.visibility = View.GONE
            mDialogView.ad_txt.visibility = View.GONE
        }
        mDialogView.main_menu_btn.setOnClickListener {
            fullscreenAdView.show()
            mAlertDialog.dismiss()
            Navigation.findNavController(view).navigateUp()
        }
    }

    fun rewardAd(view: View) {
        var rewardedAd: RewardedAd =
            RewardedAd(context, "ca-app-pub-1898235865477290/4302580318")
        val adLoadCallback = object : RewardedAdLoadCallback() {
            override fun onRewardedAdLoaded() {
                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
                showAd(rewardedAd)
            }

            override fun onRewardedAdFailedToLoad(errorCode: Int) {
                Toast.makeText(context, getString(R.string.reward_ad_failed_to_load), Toast.LENGTH_SHORT).show()
                blurLayout.visibility = View.GONE
                progressBar.stopAnim()
                resumeTimer()
            }
        }
        val adRequest: AdRequest
//        if (BuildConfig.DEBUG) {
//            adRequest = AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build()
//        } else {
            adRequest = AdRequest.Builder().build()
//        }

        rewardedAd.loadAd(adRequest, adLoadCallback)
    }

    private fun showAd(rewardedAd: RewardedAd) {
        val adCallback = object : RewardedAdCallback() {
            override fun onUserEarnedReward(p0: com.google.android.gms.ads.rewarded.RewardItem) {

                Toast.makeText(context, resources.getString(R.string.earned_reward), Toast.LENGTH_SHORT)
                    .show()
                pass++
                passTxt.text = "$pass"

            }

            override fun onRewardedAdOpened() {
            }

            override fun onRewardedAdClosed() {
                resumeTimer()

            }

            override fun onRewardedAdFailedToShow(errorCode: Int) {
                fullscreenAdView.show()
                Navigation.findNavController(view!!).navigateUp()
            }
        }
        rewardedAd.show(context!! as Activity?, adCallback)
    }

    private fun interstitalAd() {
        fullscreenAdView = InterstitialAd(context)
        fullscreenAdView.adUnitId = "ca-app-pub-1898235865477290/8616890536"
        val adRequest: AdRequest
//        if (BuildConfig.DEBUG) {
//            adRequest = AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build()
//        } else {
            adRequest = AdRequest.Builder().build()
//        }
       fullscreenAdView.loadAd(adRequest)

    }

    override fun onCardDisappeared(view: View?, position: Int) {
        val textView = view?.findViewById<TextView>(R.id.item_name_eng)
        Log.d("CardStackView", "onCardDisappeared: ($position) ${textView?.text}")
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
        Log.d("CardStackView", "onCardDragging: d = ${direction?.name}, r = $ratio")
    }

    override fun onCardSwiped(direction: Direction?) {
        Log.d("CardStackView", "onCardSwiped: p = ${manager.topPosition}, d = $direction")
        countDownTimer.cancel()
        startTimer()
        listSize--

        if (direction?.name == "Left") {
            swipeCount++
            pass--
            if(pass < 0)
                pass = 0
            passTxt.text = pass.toString()
            if (pass <= 0) {
                if (listSize != 0)
                    if (Locale.getDefault().language == "tr"){
                        view?.let {
                            finishDialog(
                                it,
                                "Pas Hakkı Bitti !" + "\nPuanınız : $score",
                                true
                            )
                        }
                    }else{
                        view?.let {
                            finishDialog(
                                it,
                                "Pass is Over !" + "\nScore : $score",
                                true
                            )
                        }
                    }

            }

        } else {
            when {
                timeRemaining > 75000.toLong() -> {
                    score += 45
                }
                timeRemaining > 60000.toLong() -> {
                    score += 40
                }
                timeRemaining > 45000.toLong() -> {
                    score += 35
                }
                timeRemaining > 30000.toLong() -> {
                    score += 30
                }
                timeRemaining > 20000.toLong() -> {
                    score += 25
                }
                timeRemaining > 10000.toLong() -> {
                    score += 20
                }
                timeRemaining > 5000.toLong() -> {
                    score += 15
                }
                timeRemaining < 5000.toLong() -> {
                    score += 10
                }
            }
        }



        if (listSize == 0) {
            if (Locale.getDefault().language == "tr")
            view?.let { finishDialog(it, "Oyun Bitti !" + "\nPuanınız : $score", false) }
            else
                view?.let { finishDialog(it, "Game Over!" + "\nScore : $score", false) }
        }
        scoreTxt.text = score.toString()
    }

    override fun onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: ${manager.topPosition}")
    }

    override fun onCardAppeared(view: View?, position: Int) {
        val textView = view?.findViewById<TextView>(R.id.item_name_eng)
        Log.d("CardStackView", "onCardAppeared: ($position) ${textView?.text}")
    }

    override fun onCardRewound() {
        Log.d("CardStackView", "onCardRewound: ${manager.topPosition}")
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fullscreenAdView.show()
                if (!Navigation.findNavController(view!!).popBackStack()) {
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }
}