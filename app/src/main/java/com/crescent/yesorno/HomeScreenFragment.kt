package com.crescent.yesorno

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.game_info_dialog.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class HomeScreenFragment : Fragment(), View.OnClickListener {
    lateinit var moviesBtn: LinearLayout
    lateinit var seriesBtn: LinearLayout
    lateinit var singersBtn: LinearLayout
    lateinit var actorsBtn: LinearLayout
    lateinit var trMoviesBtn: Button
    lateinit var frgnMoviesBtn: Button
    lateinit var trSeriesBtn: Button
    lateinit var frgnSeriesBtn: Button
    lateinit var trSingersBtn: Button
    lateinit var frgnSingersBtn: Button
    lateinit var trActorsBtn: Button
    lateinit var frgnActorsBtn: Button
    lateinit var moviesExpandable: ExpandableLayout
    lateinit var seriesExpandable: ExpandableLayout
    lateinit var singersExpandable: ExpandableLayout
    lateinit var actorsExpandable: ExpandableLayout
    private var doubleBackToExitPressedOnce = false
    lateinit var firstIntroductionPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var orTxt: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)


        initView(view)
        firstIntroductionPreferencesDialog()
        backPressHandler()
        return view
    }

    private fun initView(view: View) {


        moviesBtn = view.findViewById(R.id.movies_btn)
        seriesBtn = view.findViewById(R.id.series_btn)
        singersBtn = view.findViewById(R.id.singers_btn)
        actorsBtn = view.findViewById(R.id.actors_btn)
        trMoviesBtn = view.findViewById(R.id.turkish_movies_btn)
        frgnMoviesBtn = view.findViewById(R.id.foreign_movies_btn)
        trSeriesBtn = view.findViewById(R.id.turkish_series_btn)
        frgnSeriesBtn = view.findViewById(R.id.foreign_series_btn)
        trSingersBtn = view.findViewById(R.id.turkish_singers_btn)
        frgnSingersBtn = view.findViewById(R.id.foreign_singers_btn)
        trActorsBtn = view.findViewById(R.id.turkish_actors_btn)
        frgnActorsBtn = view.findViewById(R.id.foreign_actors_btn)
        orTxt = view.findViewById(R.id.or_txt)

        moviesExpandable = view.findViewById(R.id.movies_expandable_view)
        seriesExpandable = view.findViewById(R.id.series_expandable_view)
        singersExpandable = view.findViewById(R.id.singers_expandable_view)
        actorsExpandable = view.findViewById(R.id.actors_expandable_view)

        firstIntroductionPreferences =
            context!!.getSharedPreferences("isFirstIntroduction", Context.MODE_PRIVATE)
        editor = firstIntroductionPreferences.edit()

        moviesBtn.setOnClickListener(this)
        seriesBtn.setOnClickListener(this)
        singersBtn.setOnClickListener(this)
        actorsBtn.setOnClickListener(this)
        trMoviesBtn.setOnClickListener(this)
        frgnMoviesBtn.setOnClickListener(this)
        trSeriesBtn.setOnClickListener(this)
        frgnSeriesBtn.setOnClickListener(this)
        trSingersBtn.setOnClickListener(this)
        frgnSingersBtn.setOnClickListener(this)
        trActorsBtn.setOnClickListener(this)
        frgnActorsBtn.setOnClickListener(this)
        if (Locale.getDefault().language == "tr")
            orTxt.visibility = View.GONE

    }
    private fun firstIntroductionPreferencesDialog() {

        if (firstIntroductionPreferences.getBoolean("isFirstIntroduction", true)) {
            infoDialog()
            editor.putBoolean("isFirstIntroduction", false)
            editor.apply()
        }

    }
    private fun infoDialog() {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.game_info_dialog, null)
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mAlertDialog.window!!.setLayout(1000, 1400)
        if (Locale.getDefault().language == "tr"){
            mDialogView.tr_ll.visibility = View.VISIBLE
            mDialogView.en_ll.visibility = View.GONE
        }else{
            mDialogView.tr_ll.visibility = View.GONE
            mDialogView.en_ll.visibility = View.VISIBLE
            //TODO Kontrol et
        }
        mDialogView.main_menu_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }
    override fun onClick(view: View?) {
        when (view?.id) {
            moviesBtn.id -> {
                if (Locale.getDefault().language == "tr") {
                    if (moviesExpandable.isExpanded)
                        moviesExpandable.collapse()
                    else
                        moviesExpandable.expand()
                } else {
                    val bundle = Bundle()
                    bundle.putString("choosenCategory", "ForeignMovies")
                    Navigation.findNavController(view)
                        .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)

                }

            }
            seriesBtn.id -> {
                if (Locale.getDefault().language == "tr") {
                    if (seriesExpandable.isExpanded)
                        seriesExpandable.collapse()
                    else
                        seriesExpandable.expand()
                } else {
                    val bundle = Bundle()
                    bundle.putString("choosenCategory", "ForeignSeries")
                    Navigation.findNavController(view)
                        .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                }

            }
            singersBtn.id -> {
                if (Locale.getDefault().language == "tr") {
                    if (singersExpandable.isExpanded)
                        singersExpandable.collapse()
                    else
                        singersExpandable.expand()
                } else {
                    val bundle = Bundle()
                    bundle.putString("choosenCategory", "ForeignSinger")
                    Navigation.findNavController(view)
                        .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                }

            }
            actorsBtn.id -> {
                if (Locale.getDefault().language == "tr") {
                    if (actorsExpandable.isExpanded)
                        actorsExpandable.collapse()
                    else
                        actorsExpandable.expand()
                } else {
                    val bundle = Bundle()
                    bundle.putString("choosenCategory", "ForeignActors")
                    Navigation.findNavController(view)
                        .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                }

            }
            trMoviesBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "TurkishMovies")
                Navigation.findNavController(view).navigate(R.id.gameScreenFragment, bundle)
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                singersExpandable.collapse()
                actorsExpandable.collapse()
            }
            frgnMoviesBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "ForeignMovies")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                singersExpandable.collapse()
                actorsExpandable.collapse()
            }
            trSeriesBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "TurkishSeries")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                seriesExpandable.collapse()
                moviesExpandable.collapse()
                singersExpandable.collapse()
                actorsExpandable.collapse()
            }
            frgnSeriesBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "ForeignSeries")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                seriesExpandable.collapse()
                moviesExpandable.collapse()
                singersExpandable.collapse()
                actorsExpandable.collapse()
            }
            trSingersBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "TurkishSinger")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                singersExpandable.collapse()
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                actorsExpandable.collapse()
            }
            frgnSingersBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "ForeignSinger")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                singersExpandable.collapse()
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                actorsExpandable.collapse()
            }
            trActorsBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "TurkishActors")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                actorsExpandable.collapse()
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                singersExpandable.collapse()
            }
            frgnActorsBtn.id -> {
                val bundle = Bundle()
                bundle.putString("choosenCategory", "ForeignActors")
                Navigation.findNavController(view)
                    .navigate(R.id.action_homeScreenFragment_to_gameScreenFragment, bundle)
                actorsExpandable.collapse()
                moviesExpandable.collapse()
                seriesExpandable.collapse()
                singersExpandable.collapse()
            }
        }
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (doubleBackToExitPressedOnce) {
                    activity!!.finishAffinity()
                }

                this@HomeScreenFragment.doubleBackToExitPressedOnce = true
                Toast.makeText(context, resources.getString(R.string.exit_msg), Toast.LENGTH_SHORT)
                    .show()

                Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 3000)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

}